# Zabbix2Vtiger AutoTicket

Automated ticket creation in Vtiger by Zabbix action

# Requirements
See `requirements.txt`

# Installation
Install dependencies with `pip install -r requirements.txt`

# Usage
### Arguments
* `--trigger-status`: trigger status, `new` or `solved`
* `--severity`: trigger severity as shown in Zabbix
* `--host`: host name in Zabbix (used only for ticket name)
* `--trigger-name`: same as `--host`
* `--organization`: organization account number in Vtiger (macro in Zabbix host)  
`python<version> <path>/bin/zbxvtg_exec.py [arg] [value]`  
IMPORTANT: ALL arguments must be specified, if there are any spaces in the value use quotation marks (`"spaced value"`)
