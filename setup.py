import pathlib
import shutil
import os


def main():
    _location = pathlib.Path(os.path.realpath(__file__)).parent
    print("Installer Zabbix-Autoticket (marcopolo)")
    folder = input("Inserire in quale cartella (percorso assoluto) salvare i file di configurazione: ")
    path = pathlib.Path(folder)
    if not path.exists():
        path.mkdir(parents=True)
    if not path.joinpath("config.json").exists():
        shutil.copy2(f"{_location}/defaultconf/config.default.json", f"{folder}/config.json")  # config.default
    if not path.joinpath("events.json").exists():
        shutil.copy2(f"{_location}/defaultconf/events.default.json", f"{folder}/events.json")  # events.default

    with open('zabbix2vtiger/globals.py', 'w') as f:
        f.write(f"FILE_PATH = r'{folder}'\n")


if __name__ == '__main__':
    main()
