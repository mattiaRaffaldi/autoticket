import argparse
import autoticket.adder.add_ticket as t_adder

if __name__ == "__main__":
    t_adder.main(argparse.ArgumentParser())
