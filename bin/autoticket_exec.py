import sys
import os
#sys.path.append('/var/lib/docker/volumes/test/_data/zabbix2vtiger-autoticket/autoticket')
sys.path.insert(1, os.path.join(sys.path[0], '..'))
import autoticket.zbxvtg
if __name__ == "__main__":
    autoticket.zbxvtg.main()
