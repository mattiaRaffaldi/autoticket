import argparse
import os
import sys

sys.path += [os.path.abspath('.')]

import zabbix2vtiger.zbxvtg

if __name__ == "__main__":
    zabbix2vtiger.zbxvtg.main(argparse.ArgumentParser())
