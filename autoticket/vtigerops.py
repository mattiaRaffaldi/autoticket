import hashlib
import json
import logging

import requests
from autoticket.errors import errs
from autoticket.models.ticket import Ticket

logger = logging.getLogger('main')
ACCEPTED_STATUS = [200, 201, 203]


class VtigerOperations:

    def __init__(self, credentials):
        self._URL = f"http://{credentials.url}/webservice.php"
        self._username = credentials.username
        self._access_key = credentials.accesskey
        #logger.debug(f"{self._username}||||{self._access_key}")
        self.session = None

    @staticmethod
    def __check_response(res: requests.Response):
        if res.status_code not in ACCEPTED_STATUS:
            logger.error(f"Code {res.status_code}")
            raise ValueError('Vtiger: Wrong response code')
        if not res.json().get('success'):
            logger.error(f"Error: {res.json()['error']['message']}")
            raise ValueError('Vtiger: call did not succeed')

    def __url_builder(self, method='GET',
                      operation=None,
                      username=None,
                      session_name=None,
                      element_type=None,
                      wbs_id=None,
                      query=None):
        """
        Build request URL based on parameters
        :param method: method type (GET, POST)
        :param operation: operation name (Challenge, ListTypes, Describe, Retrieve, Query)
        :param username: username
        :param session_name: sessionName from Login
        :param element_type: desired Vtiger module
        :param wbs_id: ID of the specific webservice (for Retrieve)
        :param query: SQL query
        :return: built URL
        """
        base_url = f"{self._URL}"
        first = True
        connector = '?'
        logger.info(f"Method is {method}, operation is {operation}")
        if method == 'POST':
            return base_url
        if operation:
            base_url = f"{base_url}{connector}operation={operation}"
            first = False
        if username:
            if not first:
                connector = '&'
            base_url = f"{base_url}{connector}username={username}"
            first = False
        if session_name:
            if not first:
                connector = '&'
            base_url = f"{base_url}{connector}sessionName={session_name}"
            first = False
        if element_type:
            if not first:
                connector = '&'
            base_url = f"{base_url}{connector}elementType={element_type}"
            first = False
        if wbs_id:
            if not first:
                connector = '&'
            base_url = f"{base_url}{connector}id={wbs_id}"
            first = False
        if query:
            if not first:
                connector = '&'
            base_url = f"{base_url}{connector}query={session_name}"
        logger.info(f"URL is {base_url}")
        return base_url

    def get_token(self):
        """
        Get initial token
        :return:
        """
        url = self.__url_builder(operation='getchallenge', username=self._username)
        res_challenge = requests.get(f"{url}")
        try:
            self.__check_response(res_challenge)
        except ValueError as e:
            raise errs.TokenError(e)
        token = res_challenge.json()['result']['token']
        return token

    def login(self, token):
        """
        Login into Vtiger API to get sessionName
        :param token: token from Challenge
        :return: sessionName
        """
        url = self.__url_builder(method='POST')
        # Calculate md5 hash of token+accessKey
        login_hash = hashlib.md5((token + self._access_key).encode('utf-8')).hexdigest()
        # Build request body
        #   operation=login
        #   username=<user>
        #   accessKey=<calculated key>
        body = {'operation': (None, 'login'), 'username': (None, self._username), 'accessKey': (None, login_hash)}
        res_login = requests.post(f"{url}", files=body)
        try:
            self.__check_response(res_login)
        except ValueError as e:
            raise errs.LoginError(e)
        result = res_login.json()['result']
        session = result['sessionName']
        logger.info(f"Logged in as {self._username}")
        self.session = session

    def logout(self):
        """
        Logout of specified session
        :param session: sessionName
        :return:
        """
        url = self.__url_builder(method='POST')
        body = {'operation': (None, 'logout'), 'sessionName': (None, self.session)}
        res_logout = requests.post(f"{url}", files=body)
        try:
            self.__check_response(res_logout)
        except ValueError as e:
            raise errs.LogoutError(e)
        logger.info("Logged out")

    def open_ticket(self, ticket: Ticket):
        """
        :param session: sessionName
        :param ticket: Ticket to open
        :return: Ticket number
        """
        url = self.__url_builder(method='POST')
        body = {'operation': (None, 'create'),
                'sessionName': (None, self.session),
                'element': (None, json.dumps(ticket.prepare())),
                'elementType': (None, 'HelpDesk')} 
        res_ticket = requests.post(f"{url}", files=body)
        try:
            self.__check_response(res_ticket)
        except ValueError as e:
            logger.warning(f"VALUE ERROR:{e}")
            raise errs.OperationError(e)
        except TypeError as t:
            logger.warning(f"raise {t} error")
        except KeyError as k:
            logger.warning(f"raise {k} error")
        r = res_ticket.json() 
        ticket_id = r['result']['id']
        ticket_num = r['result']['ticket_no']
        #cf_1246 = r['result']['cf_1246']
        logger.info(f"Ticket {ticket_num} created")
        return {'ticket_no': ticket_num, 'id': ticket_id}

    def update_ticket(self, ticket: Ticket):
        t = json.dumps(ticket.prepare())
        logger.debug(f"TICKET TO SEND:{t}")
        url = self.__url_builder(method='POST')
        body = {'operation': ( 'update'),
                'sessionName': (None, self.session),
                'element': (None, json.dumps(ticket.prepare()))}
        try:
            res_ticket = requests.post(f"{url}", files=body)
        except  Exception as err:
            logger.warning(f"ERROR : {err}")

        try:
            self.__check_response(res_ticket)
        except ValueError as e:
            raise errs.OperationError(e)
        t_num = res_ticket.json()['result']['ticket_no']
        logger.info(f"Ticket {t_num} updated successfully")
        return t_num

     #metodo che prende in input il ticket_id il cui stato è da modificare
    def revise_ticket(self,ticket_id,status):
        logger.debug(f"TICKETID: {ticket_id}| status: {status}")
        
        tosend = Ticket.reviseBody(ticket_id,status)
        url = self.__url_builder(method='POST')
        
        body = {'operation': (None, 'revise'),
                'sessionName': (None, self.session),
                'element': (None, json.dumps(tosend))}
        try:
            res_ticket = requests.post(f"{url}", files=body)
        except  Exception as err:
            logger.warning(f"ERROR : {err}")

        try:
            self.__check_response(res_ticket)
        except ValueError as e:
            raise errs.OperationError(e)
        t_num = res_ticket.json()['result']['ticket_no']
        logger.info(f"Ticket {t_num} updated successfully")
        return t_num

    def add_comment(self, ticket_id, comment, user_id):
        """
        :param session: sessionName
        :param ticket: Ticket to open
        :return: Ticket number
        """
        url = self.__url_builder(method='POST')
        tosend = Ticket.addComment(ticket_id,comment,user_id)

        body = {'operation': (None, 'create'),
                'sessionName': (None, self.session),
                'element': (None, json.dumps(tosend)),
                'elementType': (None, 'ModComments')} 

        comm = requests.post(f"{url}", files=body)
        try:
            self.__check_response(comm)
        except ValueError as e:
            logger.warning(f"VALUE ERROR:{e}")
            raise errs.OperationError(e)
        except TypeError as t:
            logger.warning(f"raise {t} error")
        except KeyError as k:
            logger.warning(f"raise {k} error")
        r =comm.json()
        logger.info(f"Comment {r['result']['id']} successfully added")
        return {"commentId":r['result']['id']}

    def test(self):
        url = self.__url_builder(method='GET', operation='listtypes', session_name=self.session)
        ret = requests.get(url)
        if ret.json().get('success') == 'false':
            return False
        return True
