import logging
import os
import sqlite3
from autoticket import config
logger = logging.getLogger('main')


class DBAccess:

    def __init__(self, sql_dir=None):
        self.connection = None
        self.sql_dir = sql_dir or "resources"  # fixme temp
        self.cf_name =  config.CfDetails().cf_name

    def connect(self, file):
        try:
            self.connection = sqlite3.connect(file)
            self.connection.row_factory = sqlite3.Row
        except sqlite3.Error as e:
            logger.error(f"{type(e).__name__}: {e.args}")
            exit(1)

    def setup(self):
        self.connection: sqlite3.Connection
        try:
            cursor = self.connection.cursor()
            with open(os.path.join(self.sql_dir, "setup.sql")) as f: 
                script = f.read()
            cursor.executescript(script)
            self.connection.commit()
        except sqlite3.Error as e:
            logger.error(f"{type(e).__name__}: {e.args}")
            raise
        if self.check_column==False:
            command = f"alter table open_tickets add column {self.cf_name} varchar(1000)"
            logger.debug(f"{command}")
            res = cursor.execute(command)
            self.connection.commit()


    def check_column(self):
        c.execute(f"SELECT EXISTS(SELECT %s FROM open_tickets)" % self.cf_name)
        if c.fetchone():
                return True
        else:
                return False

    def _get_cursor(self) -> sqlite3.Cursor:
        self.connection: sqlite3.Connection
        try:
            cursor = self.connection.cursor()
        except sqlite3.Error as e:
            logger.error(f"{type(e).__name__}: {e.args}")
            raise
        return cursor

    def _get_commands(self, op, table):
        with open(os.path.join(self.sql_dir, f"{op}_{table}.sql"), 'r') as fd:
            sql_file = fd.read()

        # all SQL commands (split on ';')
        commands = sql_file.split(';')
        return commands

    def add_item_old(self, table, item_id=None, **parameters):

        cursor = self._get_cursor()
        commands = self._get_commands('insert', table)
        print(parameters)
        # Execute every command from the input file
        for command in commands:
            try:
                print(command)
                cursor.execute(command, parameters)
            except sqlite3.Error as e:
                print(e)
                pass
        self.connection.commit()

    def add_item(self, table, item_id=None, **parameters):
        cursor = self._get_cursor()
        columns = ', '.join(parameters.keys())
        placeholders = ':' + ', :'.join(parameters.keys())
        logger.debug(f"PLACEHOLDERS: {placeholders}")
        query = f"INSERT INTO {table} (%s) VALUES (%s)" % (columns, placeholders)
        
        res = cursor.execute(query, parameters)
        self.connection.commit()

    def get_item(self, table, item_id=None, **parameters) -> list:
        cursor = self._get_cursor()
        query = "SELECT * FROM {table}{condition};"
        if item_id:
            condition = f" WHERE event_id=?"
            query = query.format(table=table, condition=condition)

            try:
                logger.info(f"event_id from db_dao: {item_id}")
                res =cursor.execute(query, (item_id,))
                #logger.info(f" TICKET RES: {cursor.fetchone()}")
                #return cursor.fetchone()
            except Exception as e:
                logger.warning(f"ERROR QUERY: {e}")
        else:
            query = query.format(table=table, condition='')
            res = cursor.execute(query)
        return [dict(row) for row in res.fetchall()]

    def update_item(self, table, item_id=None, **parameters):
        cursor = self._get_cursor()
        query = f"UPDATE {table} SET "
        for p in parameters:
            query += f"{p}=:{p}, "
        query = query[:-2]
        query += " WHERE event_id=:event_id"
        params = {**parameters, **{'event_id': item_id}}
        print(query)
        print(params)
        res = cursor.execute(query, params)
        self.connection.commit()
        pass

    def delete_item(self, table, item_id):
        #logger.debug(f"eliminare: {item_id}")
        cursor = self._get_cursor()
        sql = f"DELETE FROM {table} WHERE event_id = ?"
        cursor.execute(sql,( item_id,))
        self.connection.commit()
