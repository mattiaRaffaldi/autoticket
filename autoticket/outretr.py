import mysql.connector
import logging

logger = logging.getLogger('main')


def __connect_db(conf):
    logger.log(logging.INFO, f"Connecting to database {conf['database']}")
    return mysql.connector.connect(**conf)


def __get_cursor(db):
    return db.cursor()


def __select_account(cursor, account_num):
    query_account = "select accountid from vtiger_account where account_no = %s"
    cursor.execute(query_account, (account_num,))
    return cursor.fetchone()

def __select_ticketid(cursor, ticket_no):
    logger.debug(f"TICKET NUMBER TO FETCH: {ticket_no}")
    query_account = "select ticketid  from vtiger_troubletickets where ticket_no = %s"
    cursor.execute(query_account, (ticket_no,))
    return cursor.fetchone()

def __closeticket_query(cursor, ticket_no,config):
    clstr = '"closed"'
    tt = (ticket_no,)
    conn = __connect_db(config.to_dict())
    query_account = "update vtiger_troubletickets set status='closed' where ticket_no = %s"
    cursor.execute(query_account,tt)
    conn.commit()

def retrieve_organization(org, config):
    conn = __connect_db(config.to_dict())
    cur = __get_cursor(conn)
    logger.info(f"cur: {cur}, org: {org}")
    try:
        res = __select_account(cur, org)[0]
        logger.info(f"Retrieved id for account {org}")
    except Exception as e:
        logger.error(f"DBError: {type(e).__name__}")
        raise
    finally:
        cur.close()
    return res

def retrieve_ticketid(org, config):
    conn = __connect_db(config.to_dict())
    cur = __get_cursor(conn)
    logger.info(f"cur: {cur}, org: {org}")
    try:
        res = __select_ticketid(cur, org)[0]
        logger.info(f"Retrieved ticketid {res}")
    except Exception as e:
        logger.error(f"DBError: {type(e).__name__}")
        raise
    finally:
        cur.close()
    return res


def close_ticket(tt, config):
    conn = __connect_db(config.to_dict())
    cur = __get_cursor(conn)
    try:
        __closeticket_query(cur, tt,config)
        logger.info(f"Retrieve executed for {tt}")
    except Exception as e:
        logger.error(f"DBError: {type(e).__name__}")
        raise
    finally:
        cur.close()
        
