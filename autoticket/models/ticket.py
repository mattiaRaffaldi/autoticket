from autoticket import config
class Ticket:
    def __init__(self,
                 event_id,
                 assigned_user_id,
                 ticketpriorities,
                 ticketstatus,
                 ticket_title,
                 url,
                 parent_id=None,
                 ticketseverities=None,
                 description=None,
                 contact_id=None,
                 ticket_no = None,
                 ticket_id = None,
                 host = None,
                 message=None,
                 **kwargs):
        """
        :param assigned_user_id: User assigned to ticket
        :param ticketpriorities: Ticket priority
        :param ticketstatus: Ticket status (new, update, etc...)
        :param ticket_title: Title
        :param parent_id: User/Organization of origin of the ticket
        :param ticketseverities: Severity
        :param description: Description
        :param contact_id: User contact
        """

        self.cf_alarmLink = config.CfDetails().cf_name
        self._event_id = event_id
        self._assigned_user_id = assigned_user_id
        self._priority = ticketpriorities
        self._status = ticketstatus
        self._title = ticket_title
        self._url = url
        # not mandatory fields
        self._parent_id = parent_id
        self._severity = ticketseverities
        self._description = description
        self._ticket_no = ticket_no
        self._ticket_id = ticket_id
        self._contact_id = contact_id
        self._host = host
        self._message = message
        # other fields
        self.additional = kwargs

    @property
    def assigned_user_id(self):
        return self._assigned_user_id
    @property
    def assigned_user_id(self):
        return self._assigned_user_id

    @property
    def ticket_no(self):
        return self._ticket_no
    @property
    def ticket_id(self):
        return self._ticket_id


    @property
    def ticketpriorities(self):
        return self._priority

    @property
    def ticketstatus(self):
        return self._status

    @property
    def ticket_title(self):
        return self._title


    @property
    def parent_id(self):
        return self._parent_id

    @property
    def ticketseverities(self):
        return self._severity

    @property
    def description(self):
        return self._description

    @property
    def host(self):
        return self._host


    @property
    def contact_id(self):
        return self._contact_id

    #SET METHODS 
    def set_ticket_num(self,ticket_n):
       self._ticket_no=ticket_n

    def set_ticket_id(self,ticket_id):
        self._ticket_id = ticket_id

    def set_message(self,message):
        self._message = message



    def set_ticketstatus(self,status):
        self._status = status

    def __str__(self):
        #TODO finish the tostring method 
        return  f"{self._status}| {self._ticket_no}|{self._event_id}|{self._host}"



    def prepare(self):

        prepared_ticket = {
            'event_id':self._event_id,
            'assigned_user_id': self._assigned_user_id,
            'ticketpriorities': self._priority,
            'ticketstatus': self._status,
            'ticket_title': self._title,
            'parent_id': f"11x{self._parent_id}",
            'ticketseverities': self._severity,
            'description': self._description,
            'contact_id': self._contact_id,
            'host': self._host,
            'ticket_no': self._ticket_no,
            'ticket_id': self._ticket_id,
            self.cf_alarmLink :self._url,
            'message' : self._message
        }
        prepared_ticket = {**prepared_ticket, **self.additional}

        return {k: v for k, v in prepared_ticket.items() if v}

    @staticmethod
    def reviseBody(ticketid,status):
        #metodo che permette di effettuare la chiusura di un ticket.
        #i parametri che vengono inseriti nel body sono  soltanto ticket_id,ticketstatus
        #per modificare altri campi, utilizzare metodo update_ticket.

        prepared_ticket = {
            'id':f'{ticketid}',
            'ticketstatus': f'{status}'
        }
        return {k: v for k, v in prepared_ticket.items() if v}

    @staticmethod
    def addComment(ticket_id,content,user_id):
        #metodo che permette di effettuare la chiusura di un ticket.
        #i parametri che vengono inseriti nel body sono  soltanto ticket_id,ticketstatus
        #per modificare altri campi, utilizzare metodo update_ticket.

         element = {
              'commentcontent' : f'{content}',
              'assigned_user_id': f'{user_id}',
              'related_to' : f'{ticket_id}'
          }
         return {k: v for k, v in element.items() if v}


