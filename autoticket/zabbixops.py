import logging

import requests

logger = logging.getLogger('main')


class ZabbixOperations:

    def __init__(self, credentials):
        self.api_url = f"http://{credentials.url}/api_jsonrpc.php" or 'http://192.168.2.20:9000/api_jsonrpc.php'
        self.username = credentials.username
        self.password = credentials.password
        self.auth = None

    def __build_body(self, method, params, auth=True):
        body = {
            'jsonrpc': '2.0',
            'method': method,
            'params': params,
            'id': 1
        }
        if auth:
            body['auth'] = self.auth
        logger.debug(f"Zabbix body: {body}")
        return body  # json.dumps(body)

    def login(self):
        if self.auth is not None:
            self.auth = None
        body = self.__build_body('user.login', {'user': self.username, 'password': self.password})
        result = requests.post(self.api_url, json=body)
#	logger.info(f"try1: {result.json()['jsonrpc']}")
     # todo try
        logger.info(f"-- LOGIN RESULTS:{result.json()}")
        logger.debug(f"result: {result.text}")
        try:
            self.auth = result.json()['result']
        except Exception as e:
            logger.error(f"Unable to login to Zabbix ({type(e).__name__})")
            raise

    def logout(self):
        body = self.__build_body('user.logout', [])
        result = requests.post(self.api_url, json=body)
        try:
            result.json()['result']
        except Exception as e:
            logger.error(f"Error during Zabbix logout ({type(e).__name__})")
            raise

    def acknowledge(self, event, ticket):
        message = f"Event {event} auto-acknowledged by ticket {ticket}"
        body = self.__build_body('event.acknowledge', {'eventids': event, 'action':4 , 'message': message})

        result = requests.post(self.api_url, json=body)

        if result.json().get('error'):
            logger.error(f"Error trying to acknowledge event {event}: {result.json().get('error').get('data')}")

    def get_all_messages(self, event):
        pass

    def test(self):
        body = self.__build_body('user.checkAuthentication', {'sessionid': self.auth}, auth=False)
        result = requests.post(self.api_url, json=body)
        try:
            userinfo = result.json()
        except Exception as e:  # fixme
            logger.error(f"Error in Zabbix session ({type(e).__name__})")
            raise
        if not userinfo:
            return False
        return True
