import json
import logging
import os
import pathlib
import sys
import threading
import time
from logging import handlers

from datetime import datetime
import pika
from autoticket import config
from autoticket.errors import errs
from autoticket import db_dao_sqlite
from autoticket import vtigerops, zabbixops, outretr
from autoticket.models.ticket import Ticket
#from autoticket import testpy
logger = logging.getLogger('main')
DEFAULT_MSG = 'This ticket has been created automatically by Monitoring System because a trigger has been enabled.'
TRIGGER_SEVERITY = {
    'Not clasisfied': -1,
    'Information': 0,
    'Warning': 1,
    'Average': 2,
    'High': 3,
    'Disaster': 4}


class AutoticketService:

    def __init__(self):
        self.vtiger_manager = vtigerops.VtigerOperations(config.VtigerConfig())
        self.zabbix_manager = zabbixops.ZabbixOperations(config.ZabbixConfig())
        dbpath = pathlib.Path(__file__).parent / 'resources'
        self.db = db_dao_sqlite.DBAccess(dbpath)
        self.lock = threading.Lock()

    def session_setup(self, session=None):
        if not session or 'vtiger' in session:
            token = self.vtiger_manager.get_token()
            self.vtiger_manager.login(token)
        if not session or 'zabbix' in session:
            self.zabbix_manager.login()

    def sessions_close(self):
        self.vtiger_manager.logout()
        self.zabbix_manager.logout()

    def test_sessions(self):
        with self.lock:
            retry = []
            if not self.vtiger_manager.test():
                retry.append('vtiger')
            if not self.zabbix_manager.test():
                retry.append('zabbix')
            self.session_setup(retry)



    def openTicket(self,ch,method,properties,body):
        with self.lock:
            data = json.loads(body)
            logger.info(f"{data}")
            event_id = data.pop('event_id')
            assigned_user_id = "20x4"  # todo generic, config?  
            ticketpriorities = data.pop('priority')
            ticketstatus = data.pop('trigger_status')
            host = data.pop('host')
            ticket_title = "Host:[" +host + '].Trigger name:[' + data.pop('trigger_name')+ "]"
            parent_id = outretr.retrieve_organization(data.pop('organization'), config.DBConfig())
            ticketseverities = data.pop('severity')
            description =  data.pop("description")
            url  =  data.pop("url")

            contact_id = "12x316135"
            # ticket = Ticket(assigned_user_id, ticketpriorities, ticketstatus, ticket_title, cf_1246, cf_1248,
            #                parent_id, ticketseverities, description, contact_id, **data)
            ticket = Ticket(event_id,
                            assigned_user_id,
                            ticketpriorities,
                            ticketstatus,
                            ticket_title,
                            url,
                            parent_id,
                            ticketseverities,
                            description,
                            contact_id,
                            **data)
            try:
                ticket_num = self.vtiger_manager.open_ticket(ticket)
                logger.debug(f"TICKET CREATED: {ticket_num}")
                tt = ticket_num.get("ticket_no")
                ticketid = ticket_num.get("id")
                #set ticket number
                ticket.set_ticket_num(tt)
                ticket.set_ticket_id(ticketid)
                logger.info(f"ticket to save:{ticket}")
                # ticket_num = int(time.time())
            except errs.OperationError:
                # todo error in vtiger, retry connection?
                logger.warning("Could not open Ticket")
                self.session_setup(session='vtiger')
                ch.basic_nack(method.delivery_tag)
                return
            except Exception as e:
                logger.warning(f"{type(e).__name__}{e.args} in Vtiger")
                ch.basic_nack(method.delivery_tag)
                #return
              
            self.db.add_item('open_tickets', event_id, **ticket.prepare())
            self.zabbix_manager.acknowledge(event_id, ticket_num)
            ch.basic_ack(method.delivery_tag)

    def closeTicket(self,ch,method,properties,body):
        with self.lock:
            data = json.loads(body)

            event_id = data.pop('event_id')
            status = data.pop('status')
            #ticketmessage = data.pop('message')
            logger.debug(f"----ticket to get:{event_id}----")
            try:
                info = self.db.get_item('open_tickets', event_id)[0]
                ticket_id = info.get("ticket_id")
                if ticket_id is None:
                    ch.basic_ack(method.delivery_tag)
                    return
            except IndexError as i:
                logger.warning(f"Item {event_id} not found, discard the ticket")
                ch.basic_ack(method.delivery_tag) 
            except Exception as e:
                # todo item not present, error?
                logger.warning(f"{type(e).__name__} while retrieving item")
                ch.basic_nack(method.delivery_tag)
                raise
               #return
            try:
                self.vtiger_manager.revise_ticket(ticket_id,status)
            except errs.OperationError as e:
                # todo error in vtiger, retry connection?
                logger.warning(f"Could not open Ticket: {e}")
                self.session_setup(session='vtiger')
                ch.basic_nack(method.delivery_tag)
                return
            except Exception as e:
                logger.warning(f"Error: {type(e).__name__}")
                ch.basic_nack(method.delivery_tag)
                return
            self.db.delete_item('open_tickets', event_id)
            ch.basic_ack(method.delivery_tag)



    def ackTicket(self,ch,method,properties,body):
         with self.lock:
            data = json.loads(body)

            event_id = data.pop('event_id')
            ticketmessage = data.pop('message')
            status  = data.pop('status')
            try:
                info = self.db.get_item('open_tickets', event_id)[0]
                
                #user_id = info.get("assigned_user_id")
                ticket_id = info.get("ticket_id")
              
                #info.get("ticketstatus") =  "Closed"
            except Exception as e:
                # todo item not present, error?
                logger.warning(f"{type(e).__name__} while retrieving item")
                ch.basic_nack(method.delivery_tag)
                raise
                #return
            try:
                self.vtiger_manager.revise_ticket(ticket_id,status)
            except errs.OperationError as e:
                # todo error in vtiger, retry connection?
                logger.warning(f"Could not open Ticket: {e}")
                self.session_setup(session='vtiger')
                ch.basic_nack(method.delivery_tag)
                return
            except Exception as e:
                logger.warning(f"Error: {type(e).__name__} {e}")
                ch.basic_nack(method.delivery_tag)
                return

            try:
                user_id = info.get("assigned_user_id")
                self.vtiger_manager.add_comment(ticket_id,ticketmessage,user_id)
            except errs.OperationError as e:
                # todo error in vtiger, retry connection?
                logger.warning(f"Could not open Ticket: {e}")
                self.session_setup(session='vtiger')
                ch.basic_nack(method.delivery_tag)
                return
            except Exception as e:
                logger.warning(f"Error: {type(e).__name__} {e}")
                self.session_setup(session='vtiger')
                ch.basic_nack(method.delivery_tag)

            ch.basic_ack(method.delivery_tag)


def session_check(service: AutoticketService, test_window=30):
    time.sleep(test_window * 60)
    while True:
        service.test_sessions()
        time.sleep(test_window*60)


def main():
    #logger.info("START")
    setup(config.GeneralConfig())
    service = AutoticketService()
    service.session_setup()
   #service.db è la variabile che permette di creare l'istanza db 
    logger.info("before db connection")
    service.db.connect(os.path.join(service.db.sql_dir, 'tickets.db'))
    service.db.setup()
    threading.Thread(target=session_check, args=(service, 30)).start()
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='192.168.2.20'))#, port=0, credentials=''))
    channel = connection.channel()
    logger.debug("before declaration")
    queues = ["new_tickets","tickets_to_close","tickets_to_ack"]

    channel.queue_declare("new_tickets")
    channel.queue_declare("tickets_to_close")
    channel.queue_declare("tickets_to_ack")
    channel.basic_consume(queue="new_tickets", auto_ack=False, on_message_callback=service.openTicket) 
    channel.basic_consume(queue="tickets_to_close", auto_ack=False, on_message_callback=service.closeTicket)
    channel.basic_consume(queue="tickets_to_ack", auto_ack=False, on_message_callback=service.ackTicket)
    logger.info("ready to consume.")
    try:
       #nella callback si fa l'apertura del ticket e il suo inserimento nel db. 
        channel.start_consuming()
    except KeyboardInterrupt:
        logger.info("Service stopped")
    except Exception as e:
        logger.warning(f"Error from main:  {type(e).__name__} ({e.args})")
    except TypeError as t:
        logger.warning(f"error {t}") 
    finally:
        logger.info("Cleanup")
        service.sessions_close()
        connection.close()


def setup(log_conf: config.GeneralConfig):
    project_level = logging.DEBUG
    if log_conf.log_ative:
        formatter = logging.Formatter('%(asctime)s | %(filename)-20s | %(levelname)-8s | %(message)s')
        handler = logging.handlers.TimedRotatingFileHandler(f'{log_conf.log_folder}/marcopolo.log',
                                                            "midnight",
                                                            interval=1,
                                                            backupCount=7)
        handler.setFormatter(formatter)
        handler.setLevel(project_level)
        logger.addHandler(handler)
        logger.setLevel(project_level)
    else:
        # tmp
        formatter = logging.Formatter('%(asctime)s | %(filename)-20s | %(levelname)-8s | %(message)s')
        console_logging = logging.StreamHandler(sys.stdout)
        console_logging.setFormatter(formatter)
        console_logging.setLevel(project_level)
        logger.addHandler(console_logging)
        logger.setLevel(project_level)
        # end temp
        # logger.disabled = True
    logger.log(logging.INFO, "########## Starting new session from test/autoticket/bin/zbxvtg ##########")
