import json


class FileUtils:
    def __init__(self, file_name):
        self.file_name = file_name
        with open(file_name, 'r') as f:
            self.file = json.load(f)

    def __del__(self):
        self.finalize()

    def finalize(self):
        with open(self.file_name, 'w') as f:
            json.dump(self.file, f, indent=4)

    def get_info(self, path):
        data = self.file

        pa = path.split('.')
        if len(pa) == 0:
            last = pa[0]
        else:
            last = pa.pop()
            for p in pa:
                data = data[p]

        return data[last]

    def write_info(self, path, new_data):
        data = self.file

        pa = path.split('.')
        if len(pa) == 0:
            last = pa[0]
        else:
            last = pa.pop()
            for p in pa:
                data = data[p]
        data[last] = new_data

    def del_info(self, path):
        data = self.file

        pa = path.split('.')
        if len(pa) == 0:
            last = pa[0]
        else:
            last = pa.pop()
            for p in pa:
                data = data[p]
        del data[last]
