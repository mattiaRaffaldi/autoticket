import os


class GeneralConfig:

    def __init__(self):
        self.log_folder = os.getenv('LOG_FOLDER', '')
        self.log_ative = ('1' == os.getenv('LOG_ACTIVE', '0')) 

class CfDetails:
    def __init__(self):
       self.cf_name = os.environ['CF_NAME']
       
    #@property
    #def cf_name(self):
    #    return self.cf_name
class VtigerConfig:

    def __init__(self):
        self.url = os.environ['VTIGER_URL']
        self.username = os.environ['VTIGER_USER']
        self.accesskey = os.environ['VTIGER_KEY']


class ZabbixConfig:

    def __init__(self):
        self.url = os.environ['ZABBIX_URL']
        self.username = os.environ['ZABBIX_USER']
        self.password = os.environ['ZABBIX_PASS']


class DBConfig:

    def __init__(self):
        self.host = os.environ['DB_HOST']
        self.username = os.environ['DB_USER']
        self.password = os.environ['DB_PASS']
        self.database = os.environ['DB_DATABASE']

    def to_dict(self):
        return {'host': self.host, 'user': self.username, 'password': self.password, 'database': self.database}
