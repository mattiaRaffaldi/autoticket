import unittest
import os

from autoticket.db_dao_sqlite import DBAccess


class TestDatabase(unittest.TestCase):

    def setUp(self) -> None:
        self.db = DBAccess('../resources')
        self.db.connect('../resources/tickets1.db')

    def test_create(self):
        self.assertTrue(os.path.exists('../resources/tickets1.db'))

    def test_setup(self):
        self.db.setup()

    def test_add(self):
        self.db: DBAccess
        self.db.add_item('open_tickets', **{'event_id': 1, 'priority': 1})

    def test_get(self):
        self.db.get_item('open_tickets', 1)

    def test_update(self):
        self.db: DBAccess
        self.db.update_item('open_tickets', 1, **{'status': 2})
        self.db.get_item('open_tickets', 1)
