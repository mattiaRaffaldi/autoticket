class BaseZbxvtgError(Exception):
    def __init__(self, msg):
        super().__init__(msg)


class LoginError(BaseZbxvtgError):
    pass


class LogoutError(BaseZbxvtgError):
    pass


class TokenError(BaseZbxvtgError):
    pass


class OperationError(BaseZbxvtgError):
    pass
