import argparse
import datetime
import json
import logging
from logging import handlers


import config
from autoticket.utils.file_utils import FileUtils
import pika


logger = logging.getLogger('main')
DEFAULT_MSG = 'This ticket has been created automatically by Monitoring System because a trigger has been enabled.'
TRIGGER_SEVERITY = {
    'Not clasisfied': -1,
    'Information': 0,
    'Warning': 1,
    'Average': 2,
    'High': 3,
    'Disaster': 4
}


def main(args: argparse.ArgumentParser):
    args.add_argument("--trigger_status")
    args.add_argument("--severity")
    args.add_argument("--host")
    args.add_argument("--trigger_name")
    args.add_argument("--organization")
    args.add_argument("--event_id")
    args.add_argument("--action")
    args.add_argument("--trigger_description")
    args.add_argument("--host_id")
    args.add_argument("--trigger_id")
    args.add_argument("--message")
    p_args = args.parse_args()
    print(p_args)

    # fixme env from container?
    #config_handler = FileUtils(f'{FILE_PATH}/config.json')
    #setup(config_handler)

    # Connessione a server RabbitMQ
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='192.168.2.20'))#, port=0, credentials=''))
    channel = connection.channel()
    queues =  ["new_tickets","tickets_to_close","tickets_to_ack"]
    for q in queues:
        print(q)
        channel.queue_declare(queue=q)
    print("declare done")

    logger.info(f"Starting with arguments: {p_args.trigger_status}, {p_args.severity}, "
                f"{p_args.host},  {p_args.organization}")

    try:
        if p_args.trigger_status == 'new':
            # Parse args
            priority = 'High' if TRIGGER_SEVERITY[p_args.severity] >= 3 else 'Normal'
            severity = 'Major' if TRIGGER_SEVERITY[p_args.severity] >= 3 else 'Minor'
            ticket_date = (datetime.datetime.now() + datetime.timedelta(days=1)).strftime("%Y-%m-%d")
            hostid =  p_args.host_id
            triggerid =  p_args.trigger_id
            zbx_url =  config.ZabbixConfig().url
            problem_url = f'''http://{zbx_url}/zabbix.php?show=3&application=&name=&inventory%5B0%5D%5Bfield%5D=type&inventory%5B0%5D%5Bvalue%5D=&evaltype=0&tags%5B0%5D%5Btag%5D=&tags%5B0%5D%5Boperator%5D=0&tags%5B0%5D%5Bvalue%5D=&show_tags=3&tag_name_format=0&tag_priority=&show_opdata=0&show_timeline=1&filter_name=&filter_show_counter=0&filter_custom_time=0&sort=clock&sortorder=DESC&age_state=0&show_suppressed=0&unacknowledged=0&compact_view=0&details=0&highlight_row=0&action=problem.view&hostids%5B%5D={hostid}&triggerids%5B%5D={triggerid}'''

            parameters = {
                          'event_id': p_args.event_id,
                          'trigger_status' : p_args.trigger_status,
                          'trigger_name' : p_args.trigger_name,
                          'host' : p_args.host,
                          'priority': priority,
                          #'status': 'new',
                          'organization': p_args.organization,
                          'severity': severity,
                          'url':problem_url,
                          'description': p_args.trigger_description

                         }

            # Aggiungi messaggio a coda
            channel.basic_publish(exchange='', routing_key=queues[0], body=json.dumps(parameters))

        elif p_args.trigger_status == 'update':

            if "closed" in p_args.action:
                parameters = {
                          'event_id': p_args.event_id,
                          'status': 'closed'
                         }
            # Aggiungi messaggio a coda
                channel.basic_publish(exchange='', routing_key=queues[1], body=json.dumps(parameters))

            if "acknowledged" in p_args.action:

                msg =  f"ack: {datetime.datetime.now()}" if not p_args.message else p_args.message
                parameters = {
                          'event_id': p_args.event_id,
                          'message': msg,
                          'status': 'in progress'
                         }
            # Aggiungi messaggio a coda
                channel.basic_publish(exchange='', routing_key=queues[2], body=json.dumps(parameters))

        else:
            raise ValueError("Trigger status not recognized")
    except Exception as e:
        logger.error(f"Generic error: {type(e).__name__} ({e.args})")
    finally:
        #config_handler.finalize()
        connection.close()
        pass


def setup(config):
    project_level = logging.DEBUG
    formatter = logging.Formatter('%(asctime)s | %(filename)-20s | %(levelname)-8s | %(message)s')
    log_folder = config.get_info('config.logfolder')
    handler = logging.handlers.TimedRotatingFileHandler(f'{log_folder}/marcopolo.log',
                                                        "midnight",
                                                        interval=1,
                                                        backupCount=7)
    handler.setFormatter(formatter)
    handler.setLevel(project_level)
    logger.addHandler(handler)
    logger.setLevel(project_level)
    logger.log(logging.INFO, "########## Starting new session from /docker/volumes/zabbix.../##########")



if __name__=="__main__":
    main(argparse.ArgumentParser())
