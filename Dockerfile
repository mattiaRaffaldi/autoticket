FROM python:3.8

RUN mkdir /opt/Autoticket

COPY autoticket /opt/Autoticket/autoticket
COPY bin /opt/Autoticket/bin
COPY requirements.txt /opt/Autoticket

RUN pip install -r /opt/Autoticket/requirements.txt

ENV PYTHONPATH "${PYTHONPATH}:/opt/Autoticket:/opt/Autoticket/autoticket"
WORKDIR /opt/Autoticket

CMD ["python", "bin/autoticket_exec.py"]
